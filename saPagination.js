angular.module('saPagination', [])
	.directive('saPagination',function(){

		var controller = function($scope,$filter){
			$scope.pageSize = 10;
			$scope.pageSizeOptions = [5,10,25,50,100,200]
			$scope.limitOne = 0;
			$scope.currentPage = 1;
			$scope.numberOfPages = null;

			$scope.previousPage = function(){
				if($scope.currentPage > 1){
					$scope.currentPage--;
				}
			}

			$scope.nextPage = function(){
				if($scope.currentPage < $scope.numberOfPages){
					$scope.currentPage++;
				}
			}

			$scope.$watch($scope.records,function(records){
				$scope.records = records;
				$scope.currentPage = 1;
				countNumberOfFilteredRecords();
				applyChangeOfPageNumber();
			});

			$scope.$watch('search',function(){
				$scope.currentPage = 1;
				countNumberOfFilteredRecords();
				applyChangeOfPageNumber();
			},true);

			$scope.$watch('currentPage',function(){
				applyChangeOfPageNumber();
			},true);

			$scope.$watch('pageSize',function(){
				countNumberOfFilteredRecords();
				ensureCurrentPageInBounds();
			})

			var countNumberOfFilteredRecords = function(){
				var filtered_records = $filter('filter')($scope.records,$scope.search);
				if(filtered_records !== undefined && filtered_records !== null){
					$scope.numberOfResults = filtered_records.length;
					$scope.numberOfPages = numberOfPages();
				}
			}

			var applyChangeOfPageNumber = function(){
				ensureCurrentPageInBounds();
				$scope.limitOne = calculateLimitOffset();
			}

			var numberOfPages = function(){
				return Math.ceil($scope.numberOfResults / $scope.pageSize);
			}

			var ensureCurrentPageInBounds = function(){
				if($scope.currentPage > $scope.numberOfPages){
					$scope.currentPage = $scope.numberOfPages;
				}
				if($scope.currentPage < 1) {
					$scope.currentPage = 1;
				}
			}

			//negative offset to determine where current page starts relative to end of the data set
			var calculateLimitOffset = function(){
				return ($scope.numberOfResults - ($scope.pageSize * ($scope.currentPage - 1))) * -1;
			}
		}

		var template = '<div>\n    <form class="form-inline">\n        <div class="row-fluid">\n            <div class="pull-right input-append input-prepend">\n                <a href="javascript:void(0)" class="btn" ng-click="previousPage()" title="Previous Page">\n                    <b class="icon-chevron-left"></b>\n                </a>\n                <input\n                        id="pageNumber"\n                        type="text"\n                        ng-model="currentPage"\n                        class="input-mini"\n                        />\n                <a class="btn disabled"> / {{ numberOfPages }}</a>\n                <a href="javascript:void(0)" class="btn" ng-click="nextPage()" title="Next Page">\n                    <b class="icon-chevron-right"></b>\n                </a>\n            </div>\n            <label for="number_of_results">Results Per Page</label>\n            <select class="input-mini" id="number_of_results" ng-model="pageSize">\n                <option value="5">5</option>\n                <option value="10">10</option>\n                <option value="25">25</option>\n                <option value="50">50</option>\n                <option value="100">100</option>\n            </select>\n        </div>\n    </form>\n    <div ng-transclude></div>\n    <p>Number of results: {{ numberOfResults }}</p>\n</div>';

		return {
			controller: controller,
			scope: {
				records: "&records",
				search: "=search"
			},
			restrict: 'A',
			replace: true,
			transclude: true,
			template: template
		}
	})